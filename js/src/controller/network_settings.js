const grpc = require('@grpc/grpc-js');
const path = require('path');
const os = require('os');

const {BrowserWindow,nativeImage,ipcMain,screen} = require('electron');

const split = require('../api/split');
const netctl = require('../api/netctl');

class NetworkSettingsController {
    constructor(netctlFrontServerAddress, netctlBackServerAddress, indexFile) {
        this.netctlFrontServerAddress = netctlFrontServerAddress;
        this.netctlBackServerAddress = netctlBackServerAddress;
        this.netctlFrontUUID = null;
        this.indexFile = indexFile;
        this.browserWindow = null;
        this.netctlFrontClient = null;
        this.netctlBackClient = null;

        switch (os.platform()) {
            case 'win32':
                this.networkSettingsIcon = path.join(__dirname, '../assets/icons/main_menu_icon.ico');
                break;

            case 'linux':
                this.networkSettingsIcon = path.join(__dirname, '../assets/icons/main_menu_icon.png');
                break;
        }

        this.bindMethods();
        this.bindRendererEvents();
    }

    bindMethods() {
        this.initNetctlFrontClient = this.initNetctlFrontClient.bind(this);
        this.initNetctlBackClient = this.initNetctlBackClient.bind(this);
        this.initNetworkInterfaceInfo = this.initNetworkInterfaceInfo.bind(this);
        this.showNetworkSettingsMenu = this.showNetworkSettingsMenu.bind(this);
        this.handleForgetNetworkEvent = this.handleForgetNetworkEvent.bind(this);
    }

    bindRendererEvents() {
        ipcMain.on('network-settings-menu-ready',
            () => this.initNetctlFrontClient(this.netctlFrontServerAddress)
        );

        ipcMain.on('network-settings-button-clicked',
            () => this.showNetworkSettingsMenu()
        );

    }

    initNetctlFrontClient(address) {
        let client = new netctl.clients.NetctlFrontClient(
            address,
            grpc.credentials.createInsecure()
        );

        grpc.waitForClientReady(client, Date.now() + (3 * 1000), (err) => {
            if (err) {
                console.log('Failed to initialize netctl client, retrying... (err=' + err + ')');
                setTimeout(() => this.initNetctlFrontClient(address), 2000);
            } else {
                this.netctlFrontClient = client;
                this.initNetworkInterfaceInfo();

                // The netctl back client is initialized here since the netctl
                // app currently is designed to support one frontend. To do
                // anything useful with netctl back, we need the fronted's UUID
                // which we can get directly once we have a frontend client.
                //
                // Eventually, if a "find all frontends" approach is used, the
                // netctl back client will be initialized first, and the metadata
                // will be used to contruct the frontend client(s) in the first place.
                let req = new split.types.GetMetaDataRequest();
                let splitClient = new split.clients.FrontendClient(
                    address,
                    grpc.credentials.createInsecure()
                );

                splitClient.getMetaData(req, (err, resp) => {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    this.netctlFrontUUID = resp.getMetadata().getUuid();
                    this.initNetctlBackClient(this.netctlBackServerAddress);
                });
            }
        });
    }

    initNetctlBackClient(address) {
        let client = new netctl.clients.NetctlBackClient(
            address,
            grpc.credentials.createInsecure()
        );

        grpc.waitForClientReady(client, Date.now() + (3 * 1000), (err) => {
            if (err) {
                console.log('Failed to initialize netctl-back client, retrying... (err=' + err + ')');
                setTimeout(() => this.initNetctlBackClient(address), 2000);
            } else {
                this.netctlBackClient = client;
                this.initSavedNetworksList()
                ipcMain.on('forget-network', this.handleForgetNetworkEvent);
            }
        });
    }

    initNetworkInterfaceInfo() {
        let req = new netctl.types.WirelessGetPropertiesRequest();

        this.netctlFrontClient.wirelessGetProperties(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            const props = resp.getProps();
            this.updateNetworkInterfaceNotify(props);
        });
    }

    initSavedNetworksList() {
        let req = new netctl.types.GetSavedNetworksRequest();
        let meta = new split.metadata.FrontendMetaData();

        meta.setUuid(this.netctlFrontUUID);
        req.setFrontend(meta);

        this.netctlBackClient.getSavedNetworks(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            let networks = [];
            resp.getNetworksList().forEach((net) => {
                const network = {
                    ssid: net.getSsid(),
                    security: net.getKeyMgmt(),
                };

                networks.push(network);
            });

            // It is possible that by the time this code is reached, the
            // browserWindow is gone, even though this code is ultimately
            // triggered when the window is opened. To be safe, and to avoid
            // annoying JS exceptions, make sure browserWindow is not null
            // before trying to trigger 'update-saved-networks-list'.
            if (this.browserWindow) {
                this.browserWindow.webContents.send('update-saved-networks-list', networks);
            } else {
                console.log('Skipping event trigger for update-saved-networks-list because browser window is null');
            }
        });
    }

    handleForgetNetworkEvent(event, arg) {
        let meta = new split.metadata.FrontendMetaData();
        meta.setUuid(this.netctlFrontUUID);

        if (arg.forgetAll) {
            let req = new netctl.types.ForgetAllNetworksRequest();
            req.setFrontend(meta);

            this.netctlBackClient.forgetAllNetworks(req, (err, resp) => {
                if (err) {
                    console.log(err);
                    return;
                }

                this.initSavedNetworksList();
            });

            return;
        }

        let req = new netctl.types.ForgetNetworkRequest();
        let cfg = new netctl.types.WirelessNetworkConfiguration();

        cfg.setSsid(arg.ssid);
        req.setNetwork(cfg);
        req.setFrontend(meta);

        this.netctlBackClient.forgetNetwork(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            this.initSavedNetworksList();
        });
    }

    updateNetworkInterfaceNotify(wirelessProperties) {
        const networkInterfaceInfo = {
            interfaceName: wirelessProperties.getIfaceName(),
            interfaceIpAddress: wirelessProperties.getIpAddress()
        };

        // It is possible that by the time this code is reached, the
        // browserWindow is gone, even though this code is ultimately
        // triggered when the window is opened. To be safe, and to avoid
        // annoying JS exceptions, make sure browserWindow is not null
        // before trying to trigger 'update-network-interface'.
        if (this.browserWindow) {
            this.browserWindow.webContents.send('update-network-interface', networkInterfaceInfo);
        } else {
            console.log('Skipping event trigger for update-network-interface because browser window is null');
        }
    }

    showNetworkSettingsMenu() {
        if (this.browserWindow) {
            this.browserWindow.focus();
            return;
        }

        const windowOptions = {
            title: 'Network Settings',
            width: 600,
            height: 600,
            show: true,
            fullscreenable: false,
            resizable: true,
            icon: this.networkSettingsIcon,
            webPreferences: {
              backgroundThrottling: false,
              nodeIntegration: true,
            }
        };

        this.browserWindow = new BrowserWindow(windowOptions);
        this.browserWindow.loadURL(this.indexFile);
        this.browserWindow.setMenu(null);
        this.browserWindow.on('close', () => this.browserWindow = null);
    }
}

module.exports = NetworkSettingsController;
