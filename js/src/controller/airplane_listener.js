const grpc = require('@grpc/grpc-js');
const path = require('path');

const netctl = require('../api/netctl');

const Registry = require('winreg');

class AirPlaneModeListener {
    constructor(netctlFrontServerAddress) {
        this.netctlFrontServerAddress = netctlFrontServerAddress;
        this.listenerInterval = 2000;
        this.airplaneStatus = null;
        this.client = null;
        // registry key location to determine the state of airplane mode within windows 10
        const keyLocation = "\\SYSTEM\\ControlSet001\\Control\\RadioManagement\\SystemRadioState";
        this.regKey = new Registry({                                       
            hive: Registry.HKLM,                                        
            key:  keyLocation
        });
        this.bindMethods();
        this.initNetctlFrontClient(this.netctlFrontServerAddress);
    }

    bindMethods() {  
        this.watchAirplaneModeRegistryValue = this.watchAirplaneModeRegistryValue.bind(this);
        this.initNetctlFrontClient = this.initNetctlFrontClient.bind(this);
        this.initAirplaneListener = this.initAirplaneListener.bind(this);
        this.callDisableInterface = this.callDisableInterface.bind(this);
        this.callEnableInterface = this.callEnableInterface.bind(this);
    }

    initNetctlFrontClient(address){
        this.client = new netctl.clients.NetctlFrontClient(
            address,
            grpc.credentials.createInsecure()
        );
        grpc.waitForClientReady(this.client, Date.now() + (3*1000), (err) => {
            if(err){
                console.log('Failed to initialize netctl client, retrying... (err=' + err + ')');
                setTimeout(() => this.initNetctlFrontClient(address), 2000);
            } else {
                //once the client is ready, begin checking for registry changes
                this.initAirplaneListener();
            }
        })
    }

    initAirplaneListener() {        
        // set Interval loop to listen to registry changes every <interval> 
        // this is an async call -> runs in the background without blocking any other tasks
        setInterval(() =>  this.watchAirplaneModeRegistryValue(), this.listenerInterval);
    }

    watchAirplaneModeRegistryValue(){
        this.regKey.values((err, items) => {
            if (err){
                console.log('Cannot access regisry key to monitor airplane mode activation. Error: ', err);
                return
            }
            //regKey returns an array of keys. 
            //Since SystemRadioState has a single value we can assume the correct value is in the 0 position of the array
            const airplaneModeEnabled = items[0].value !== '0x0';
     
            // Our state is out of sync with the registry value. Update accordingly! 
            if (this.airplaneStatus === null || this.airplaneStatus !== airplaneModeEnabled) {
                this.airplaneStatus = airplaneModeEnabled;
                this.airplaneStatus ? this.callDisableInterface() : this.callEnableInterface();
            }
        });
    }

    // disable the interface via gRPC 
    callDisableInterface(){
        let disableReq = new netctl.types.DisableInterfaceRequest();
    
        this.client.disableInterface(disableReq, (err, resp) => {
            if (err){
                console.log(err);
                return
            }
            console.log('Disabled Interface');
        })   
    }

    // enable the interface via gRPC 
    callEnableInterface(client){
        let enableReq = new netctl.types.EnableInterfaceRequest();
    
        this.client.enableInterface(enableReq, (err, resp) => {
            if (err){
                console.log(err);
                return
            }
            console.log('Enabled Interface');
        })
    }
}

module.exports = AirPlaneModeListener;
