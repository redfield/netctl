import React, {Component} from 'react';

import Button from './button';
import InputField from './input_field';

// HiddenNetworkDialog is the dialog used for hidden SSIDs.
export default class HiddenNetworkDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ssid: undefined
        };

        this.onClickNext = this.onClickNext.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onChangeSSIDField = this.onChangeSSIDField.bind(this);
    }

    onChangeSSIDField(value) {
        this.setState((state) => ({...state, ssid: value}));
    }

    onClickNext(event) {
        event.stopPropagation();

        this.props.submitSSID(this.state.ssid);
    }

    onKeyPress(event) {
        if (event.key !== 'Enter') {
            return;
        }

        event.stopPropagation();

        // If the user hits enter, it's logically the
        // same as clicking 'Next.'
        this.props.submitSSID(this.state.ssid);
    }

    render() {
        let component = (
            <div className={'network-authentication-dialog'}>
                <InputField
                    onChange={this.onChangeSSIDField}
                    onKeyPress={this.onKeyPress}
                    value={this.state.ssid}
                    label={'Enter the network name (SSID)'}
                />
                <div className={'dialog-nav-row'}>
                    <Button
                        onClick={this.onClickNext}
                        className={'dialog-nav-button'}>
                        Next
                    </Button>
                    <Button
                        onClick={this.props.onClickCancel}
                        className={'dialog-nav-button'}>
                        Cancel
                    </Button>
                </div>
            </div>
        );

        return component;
    }
}
