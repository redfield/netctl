// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React from 'react';

const Button = (props) => (
    <button {...props} className={props.className ? props.className: 'button'}>{props.children}</button>
);

export default Button;
