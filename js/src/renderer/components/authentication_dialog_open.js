import React, {Component} from 'react';

// AuthenticationDialogOpen is the dialog used for open networks.
export default class AuthenticationDialogOpen extends Component {
    constructor(props) {
        super(props);

        this.props.doConnectionAttempt();
    }

    render() {
        return null;
    }
}
