// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/split/metadata"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

var (
	// Global backend and frontend clients
	bc  *api.BackendClient
	nfc *api.FrontendClient
	nf  *metadata.FrontendMetaData

	// Tabwriter for output
	writer = tabwriter.NewWriter(os.Stdout, 8, 8, 2, '\t', tabwriter.AlignRight)
)

const (
	bashCompletion = `__netctl_custom_func() {
    case ${last_command} in
	netctl_wifi_connect)
	    __netctl_wifi_network_nouns
	    return
	    ;;
	*)
	    ;;
    esac
}

__netctl_wifi_network_nouns() {
    local network_nouns
    if network_nouns=$(netctl wifi list --no-header 2>/dev/null); then
	out=($(echo "${network_nouns}" | awk '{print $1}'))
        COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
    fi
}

__netctl_wired_saved_confs() {
    local saved_confs
    if saved_confs=$(netctl wired list-saved --no-header 2>/dev/null); then
        out=($(echo "${saved_confs}" | awk '{print $1}'))
        COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
    fi
}
`
)

var (
	rootCmd = &cobra.Command{
		Use:                    "netctl",
		Short:                  "Redfield netctl client",
		SilenceErrors:          true,
		SilenceUsage:           true,
		BashCompletionFunction: bashCompletion,
	}

	bashCompletionCmd = &cobra.Command{
		Use:    "completion",
		Short:  "Generate bash completion scripts",
		Hidden: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			err := rootCmd.GenBashCompletion(os.Stdout)
			if err != nil {
				return errors.Wrap(err, "failed to generate bash completion scripts")
			}

			return nil
		},
	}

	listFrontendsCmd = &cobra.Command{
		Use:   "list",
		Short: "List registered frontends",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			frontends, err := bc.GetAllFrontends()
			if err != nil {
				return errors.Wrap(err, "failed to get registered frontends")
			}

			header := formatTableHeader("Type", "UUID")
			fmt.Fprintln(writer, header)

			for _, frontend := range frontends {
				line := formatTableLine(frontend.Type, frontend.Uuid)
				fmt.Fprintln(writer, line)
			}

			writer.Flush()

			return nil
		},
	}

	interfaceCmd = &cobra.Command{
		Use:   "interface",
		Short: "Control the network interface managed by netctl",
	}

	interfaceEnableCmd = &cobra.Command{
		Use:   "enable",
		Short: "Enable the network interface",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return nfc.EnableInterface()
		},
	}

	interfaceDisableCmd = &cobra.Command{
		Use:   "disable",
		Short: "Disable the network interface",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return nfc.DisableInterface()
		},
	}

	modemCmd = &cobra.Command{
		Use:   "modem",
		Short: "Perform modem-specific commands",
	}

	modemConnectCmd = &cobra.Command{
		Use:   "connect",
		Short: "Connect the modem to a network",
		Args:  cobra.ExactArgs(1),
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_MODEM)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			apn := args[0]
			user, password := viper.GetString("user"), viper.GetString("password")

			cfg := &api.BearerConfiguration{
				Apn:      apn,
				User:     user,
				Password: password,
			}

			return nfc.ModemConnect(cfg)
		},
	}

	modemDisconnectCmd = &cobra.Command{
		Use:   "disconnect",
		Short: "Disconnect from the current network",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_MODEM)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return nfc.ModemDisconnect()
		},
	}

	modemStatusCmd = &cobra.Command{
		Use:   "status",
		Short: "Status of modem frontend",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_MODEM)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.ModemGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get modem properties")
			}

			columns := []string{"State", "APN"}
			items := []string{props.State, props.Apn}

			header := formatTableHeader(columns...)
			line := formatTableLine(items...)

			fmt.Fprintln(writer, header)
			fmt.Fprintln(writer, line)

			writer.Flush()

			return nil
		},
	}

	wiredCmd = &cobra.Command{
		Use:   "wired",
		Short: "Perform wired-specific commands",
	}

	wiredStatusCmd = &cobra.Command{
		Use:   "status",
		Short: "Status of wired frontend",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRED)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.WiredGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get wired properties")
			}

			columns := []string{"State", "IP Address"}
			items := []string{props.State.String(), props.IpAddress}

			header := formatTableHeader(columns...)
			line := formatTableLine(items...)

			fmt.Fprintln(writer, header)
			fmt.Fprintln(writer, line)

			writer.Flush()

			return nil
		},
	}

	wiredAuthCmd = &cobra.Command{
		Use:   "authenticate",
		Short: "Authenticate the wired interface with 802.1x",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRED)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			if saved := viper.GetString("saved"); saved != "" {
				found, err := nfc.WiredAuthenticate8021XWithSaved(saved)
				if err != nil {
					return err
				}

				if found {
					fmt.Printf("Successfully authenticated with configuration %q\n", saved)
					return nil
				}

				return errors.Errorf("no saved configuration for %q", saved)
			}

			eap := viper.GetString("eap")
			if eap == "" {
				return errors.New("EAP method is required for 802.1x authentication")
			}

			conf := &api.Wired8021XConfiguration{
				Eap:     eap,
				KeyMgmt: "IEEE8021X",
				Name:    viper.GetString("name"),
			}

			switch eap = strings.ToUpper(eap); eap {
			case wireless.EAPMethodTLS:
				id := viper.GetString("identity")
				ca := viper.GetString("ca-cert")
				client := viper.GetString("client-cert")
				key := viper.GetString("private-key")
				p12 := viper.GetString("pkcs12")
				p12pass := viper.GetString("pkcs12-password")

				if id == "" {
					return errors.New("EAP identity is required for EAP-TLS")
				}
				conf.Identity = id

				if ca == "" {
					return errors.New("CA cert is required for EAP-TLS")
				}

				data, err := ioutil.ReadFile(ca)
				if err != nil {
					return errors.Wrap(err, "reading CA cert")
				}
				conf.CaCert = data

				// Legcay PEM encryption is not supported - users should use PKCS #12
				// bundles instead. If one is not specified, expect a client cert and
				// private key file. Otherwise, just pass the bundle and optionally
				// a password for the bundle.
				if p12 != "" {
					data, err := ioutil.ReadFile(p12)
					if err != nil {
						return errors.Wrap(err, "reading PKCS #12 bundle")
					}

					conf.PrivKey = data
					conf.PrivKeyPasswd = p12pass

					// We have all we need now!
					break
				}

				if client == "" {
					return errors.New("client cert is required for EAP-TLS if not using PKC12 #12")
				}

				data, err = ioutil.ReadFile(client)
				if err != nil {
					return errors.Wrap(err, "reading client cert")
				}
				conf.ClientCert = data

				if key == "" {
					return errors.New("private key is required for EAP-TLS if not using PKC12 #12")
				}

				data, err = ioutil.ReadFile(key)
				if err != nil {
					return errors.Wrap(err, "reading private key")
				}
				conf.PrivKey = data

			case wireless.EAPMethodPEAP:
				id := viper.GetString("identity")
				pass := viper.GetString("password")
				ca := viper.GetString("ca-cert")

				if id == "" {
					return errors.New("EAP identity is required for EAP-PEAP")
				}

				if pass == "" {
					return errors.New("EAP password is required for EAP-PEAP")
				}

				if ca == "" {
					return errors.New("CA cert is required for EAP-PEAP")
				}

				conf.Identity = id
				conf.Password = pass
				conf.EapPhase1 = "peapver=0"
				conf.EapPhase2 = "auth=MSCHAPV2"

				data, err := ioutil.ReadFile(ca)
				if err != nil {
					return errors.Wrap(err, "reading CA cert")
				}
				conf.CaCert = data
			default:
				return errors.Errorf("unsupported EAP method %q", eap)
			}

			state, err := nfc.WiredAuthenticate8021X(conf)
			if err != nil {
				return errors.Wrap(err, "error in authentication attempt")
			}

			switch state {
			case api.WiredState_WIRED_CONNECTED:
				fmt.Println("Successfully authenticated.")
			case api.WiredState_WIRED_FAILED:
				return errors.New("failed to authenticate: check authentication data")
			}

			return nil
		},
	}

	wiredListSavedCmd = &cobra.Command{
		Use:   "list-saved",
		Short: "List saved 802.1x configurations for a frontend",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRED)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			confs, err := bc.GetSavedWired8021XConfs(nf)
			if err != nil {
				return err
			}

			if !viper.GetBool("no-header") {
				header := formatTableHeader("Name", "Identity", "EAP Method")
				fmt.Fprintln(writer, header)
			}

			for _, conf := range confs {
				line := formatTableLine(conf.Name, conf.Identity, conf.Eap)
				fmt.Fprintln(writer, line)
			}

			writer.Flush()

			return nil
		},
	}

	wifiCmd = &cobra.Command{
		Use:   "wifi",
		Short: "Perform wireless-specific commands",
	}

	wifiListCmd = &cobra.Command{
		Use:   "list",
		Short: "List available wireless networks",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.WirelessGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get wireless properties")
			}

			if !viper.GetBool("no-header") {
				header := formatTableHeader("SSID", "Security", "Signal")
				fmt.Fprintln(writer, header)
			}

			for _, ap := range props.GetAccessPoints() {
				// Do not show hidden networks.
				if strings.TrimSpace(ap.Ssid) == "" {
					continue
				}

				line := formatTableLine(ap.Ssid, ap.KeyMgmt, ap.SignalStrength)
				fmt.Fprintln(writer, line)
			}

			writer.Flush()

			return nil
		},
	}

	wifiConnectCmd = &cobra.Command{
		Use:   "connect",
		Short: "Connect to a wireless network",
		Args:  cobra.RangeArgs(1, 2),
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			hidden, disableAuto := viper.GetBool("hidden"), viper.GetBool("disable-autoconnect")

			if eap := viper.GetString("eap"); eap != "" {
				switch eap = strings.ToUpper(eap); eap {
				case wireless.EAPMethodTLS:
					id := viper.GetString("identity")
					ca := viper.GetString("ca-cert")
					client := viper.GetString("client-cert")
					key := viper.GetString("private-key")
					p12 := viper.GetString("pkcs12")
					p12pass := viper.GetString("pkcs12-password")

					if id == "" {
						return errors.New("EAP identity is required for EAP-TLS")
					}

					if ca == "" {
						return errors.New("CA cert is required for EAP-TLS")
					}

					// Legcay PEM encryption is not supported - users should use PKCS #12
					// bundles instead. If one is not specified, expect a client cert and
					// private key file. Otherwise, just pass the bundle and optionally
					// a password for the bundle.
					if p12 != "" {
						return connectWirelessEAPTLS(args[0], id, ca, "", p12, p12pass, hidden, disableAuto)
					}

					if client == "" {
						return errors.New("client cert is required for EAP-TLS if not using PKC12 #12")
					}

					if key == "" {
						return errors.New("private key is required for EAP-TLS if not using PKC12 #12")
					}

					return connectWirelessEAPTLS(args[0], id, ca, client, key, "", hidden, disableAuto)

				case wireless.EAPMethodPEAP:
					id := viper.GetString("identity")
					pass := viper.GetString("password")
					ca := viper.GetString("ca-cert")

					if id == "" {
						return errors.New("EAP identity is required for EAP-PEAP")
					}

					if pass == "" {
						return errors.New("EAP password is required for EAP-PEAP")
					}

					if ca == "" {
						return errors.New("CA cert is required for EAP-PEAP")
					}

					return connectWirelessEAPPEAP(args[0], id, pass, ca, hidden, disableAuto)
				default:
					return errors.Errorf("unsupported EAP method %q", eap)
				}
			}

			// Args will be length 1 or 2
			if len(args) == 1 {
				// First, check if the network name matches any saved
				// networks. If not, assume this an attempt to connect
				// to a hidden network.
				if connectSavedNetwork(args[0]) {
					return nil
				}

				connectWirelessOpen(args[0], hidden, disableAuto)
			} else {
				connectWirelessPSK(args[0], args[1], hidden, disableAuto)
			}

			return nil
		},
	}

	wifiDisconnectCmd = &cobra.Command{
		Use:   "disconnect",
		Short: "Disconnect from the current network",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return nfc.WirelessDisconnect()
		},
	}

	wifiStatusCmd = &cobra.Command{
		Use:   "status",
		Short: "Status of wireless frontend",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return frontendPreRun(cmd, api.InterfaceType_WIRELESS)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			props, err := nfc.WirelessGetProperties()
			if err != nil {
				return errors.Wrap(err, "failed to get wireless properties")
			}

			columns := []string{"State", "SSID", "IP Address", "Signal"}
			items := []string{props.State.String(), props.Ssid, props.IpAddress, props.SignalStrength}

			if props.State == api.WirelessState_PORTAL {
				columns = append(columns, "Portal URL")
				items = append(items, props.PortalUrl)
			}

			header := formatTableHeader(columns...)
			line := formatTableLine(items...)

			fmt.Fprintln(writer, header)
			fmt.Fprintln(writer, line)

			writer.Flush()

			return nil
		},
	}

	wifiImportCmd = &cobra.Command{
		Use:   "import",
		Short: "Import a pre-defined network configuration",
		Args:  cobra.ExactArgs(1),
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			uuid := viper.GetString("uuid")
			if uuid == "" {
				return errors.New("must specify UUID of frontend")
			}

			data, err := ioutil.ReadFile(args[0])
			if err != nil {
				return errors.Wrap(err, "could not read configuration file")
			}

			var cfg api.WirelessNetworkConfiguration

			if err := json.Unmarshal(data, &cfg); err != nil {
				return errors.Wrap(err, "could not parse configuration")
			}

			meta := &metadata.FrontendMetaData{
				Uuid: uuid,
			}

			if err := bc.SaveNetwork(meta, &cfg); err != nil {
				return errors.Wrap(err, "failed to import configuration")
			}

			if !viper.GetBool("connect") {
				return nil
			}

			nf, err = bc.GetFrontend(uuid)
			if err != nil {
				return errors.Wrap(err, "failed to find frontend")
			}

			// Assign global variable 'nfc'
			nfc, err = newFrontendClient(nf)
			if err != nil {
				return errors.Wrap(err, "failed to create frontend client")
			}

			doConnect(&cfg)

			return nil
		},
	}

	wifiForgetCmd = &cobra.Command{
		Use:   "forget",
		Short: "Forget a saved network configuration",
		Args:  cobra.ExactArgs(1),
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return preRun(cmd)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			uuid := viper.GetString("uuid")
			if uuid == "" {
				return errors.New("must specify UUID of frontend")
			}

			cfg := api.WirelessNetworkConfiguration{
				Ssid: args[0],
			}

			meta := &metadata.FrontendMetaData{
				Uuid: uuid,
			}

			if err := bc.ForgetNetwork(meta, &cfg); err != nil {
				return errors.Wrap(err, "failed to remove configuration")
			}

			return nil
		},
	}
)

func formatTableHeader(columns ...string) string {
	header := strings.Join(columns, "\t")

	divider := make([]string, len(columns))
	for i, col := range columns {
		divider[i] = strings.Repeat("-", len(col))
	}

	return fmt.Sprintf("%s\n%s", header, strings.Join(divider, "\t"))
}

func formatTableLine(items ...string) string {
	line := strings.Repeat("%s\t", len(items))

	args := make([]interface{}, len(items))

	for i, item := range items {
		if strings.TrimSpace(item) == "" {
			item = "-"
		}

		args[i] = item
	}

	return fmt.Sprintf(line, args...)
}

func preRun(cmd *cobra.Command) error {
	var err error

	if err = viper.BindPFlags(cmd.Flags()); err != nil {
		return errors.Wrap(err, "failed to bind flags")
	}

	// Assigning to global var
	bc, err = newBackendClient(viper.GetString("address"))
	if err != nil {
		return errors.Wrap(err, "failed to create backend client")
	}

	return nil
}

func frontendPreRun(cmd *cobra.Command, ftype api.InterfaceType) error {
	var err error

	if err := preRun(cmd); err != nil {
		return err
	}

	// If the UUID is not set, look at active frontends. If there
	// is exactly one frontend with this type, just use that UUID.
	// Otherwise, require that one is explicitly set.
	uuid := viper.GetString("uuid")

	if uuid == "" {
		frontends, err := bc.GetAllFrontends()
		if err != nil {
			return errors.Wrap(err, "failed to enumerate frontends")
		}

		for _, f := range frontends {
			if f.Type != ftype.String() {
				continue
			}

			if uuid != "" {
				return errors.Wrapf(err, "multiple %s frontends exist, specify with --uuid", ftype)
			}

			uuid = f.Uuid
		}

		if uuid == "" {
			return errors.Errorf("no active %s frontends", ftype)
		}
	}

	nf, err = bc.GetFrontend(uuid)
	if err != nil {
		return errors.Wrap(err, "failed to find frontend")
	}

	// Assign global variable 'nfc'
	nfc, err = newFrontendClient(nf)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}

	return nil
}

func init() {
	viper.SetEnvPrefix("NETCTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	// Root command
	rootCmd.PersistentFlags().String("address", "tcp://:50051", "Server address (tcp://host:port or unix:///path)")

	wifiConnectCmd.PersistentFlags().Bool("hidden", false, "Connect to network with hidden SSID")
	wifiConnectCmd.PersistentFlags().Bool("disable-autoconnect", false, "Do not connect to this network automatically when available")
	wifiConnectCmd.PersistentFlags().String("eap", "", "EAP method used for WPA-EAP authentication")
	wifiConnectCmd.PersistentFlags().String("identity", "", "EAP identity used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("password", "", "EAP password used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("ca-cert", "", "CA certificate used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("client-cert", "", "Client certificate used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("private-key", "", "Private key associated with client certificate used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("pkcs12", "", "PKCS #12 bundle contiaining private key and x509 client certificate used for WPA-EAP")
	wifiConnectCmd.PersistentFlags().String("pkcs12-password", "", "Password, if applicable, for the PKCS #12 bundle used for WPA-EAP")

	rootCmd.AddCommand(listFrontendsCmd, wifiCmd, bashCompletionCmd, interfaceCmd, modemCmd, wiredCmd)

	// Interface command
	interfaceCmd.AddCommand(interfaceEnableCmd, interfaceDisableCmd)
	interfaceCmd.PersistentFlags().String("uuid", "", "UUID for netctl frontend")

	// Wifi command
	wifiCmd.PersistentFlags().String("uuid", "", "UUID for wireless frontend")

	wifiListCmd.PersistentFlags().Bool("no-header", false, "Do not display WiFi list header")

	wifiImportCmd.PersistentFlags().Bool("connect", false, "Connect to the network specified in the configuration")

	wifiCmd.AddCommand(wifiListCmd, wifiConnectCmd, wifiDisconnectCmd, wifiStatusCmd, wifiImportCmd, wifiForgetCmd)

	// Modem command
	modemCmd.AddCommand(modemConnectCmd, modemDisconnectCmd, modemStatusCmd)
	modemCmd.PersistentFlags().String("uuid", "", "UUID for netctl frontend")

	modemConnectCmd.PersistentFlags().String("user", "", "User name, if any, required by the network")
	modemConnectCmd.PersistentFlags().String("password", "", "Password, if any, required by the network")

	// Wired command
	wiredCmd.AddCommand(wiredStatusCmd, wiredAuthCmd, wiredListSavedCmd)
	wiredCmd.PersistentFlags().String("uuid", "", "UUID for netctl frontend")

	wiredAuthCmd.PersistentFlags().String("name", "", "Name of the 802.1x configuration")
	wiredAuthCmd.PersistentFlags().String("eap", "", "EAP method used for WPA-EAP authentication")
	wiredAuthCmd.PersistentFlags().String("identity", "", "EAP identity used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("password", "", "EAP password used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("ca-cert", "", "CA certificate used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("client-cert", "", "Client certificate used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("private-key", "", "Private key associated with client certificate used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("pkcs12", "", "PKCS #12 bundle contiaining private key and x509 client certificate used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("pkcs12-password", "", "Password, if applicable, for the PKCS #12 bundle used for WPA-EAP")
	wiredAuthCmd.PersistentFlags().String("saved", "", "Name of a saved 802.1x configuration to use to authenticate")
	wiredAuthCmd.Flag("saved").Annotations = map[string][]string{
		// Tell cobra to use this bash function for completing --saved
		cobra.BashCompCustom: {"__netctl_wired_saved_confs"},
	}

	wiredListSavedCmd.PersistentFlags().Bool("no-header", false, "Do not display 802.1x configuration list header")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println("Error: ", err)
		os.Exit(-1)
	}
}
