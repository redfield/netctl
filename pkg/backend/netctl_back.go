// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package backend

import (
	"context"
	"log"
	"sync"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split"
	"gitlab.com/redfield/split/metadata"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/ui"
)

// Backend implements a netctl_back service.
type Backend struct {
	backend *split.Backend

	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedNetctlBackServer

	// Maintain a map of registered frontends, and
	// one for their associated UI proxies. Protect access to
	// the maps with a lock.
	mux       sync.Mutex
	uiProxies map[string]ui.Proxy

	// Database for the service
	*database

	// Backend options
	*serverOptions
}

// NewBackend returns a new netctl_back service.
func NewBackend(addr string, opts ...ServerOption) (*Backend, error) {
	b := &Backend{
		uiProxies:     make(map[string]ui.Proxy),
		serverOptions: &serverOptions{},
	}

	for _, opt := range opts {
		opt.apply(b.serverOptions)
	}

	bopts := make([]split.BackendOption, 0)
	if b.localListenAddr != "" {
		bopts = append(bopts, split.WithSecondListener(b.localListenAddr))
	}

	backend, err := split.NewBackend(addr, bopts...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create backend")
	}
	b.backend = backend

	b.setRegisterHandler()
	b.setUnregisterHandler()

	if !b.withoutDatabase {
		db, err := createDatabase(b.databasePath)
		if err != nil {
			return nil, errors.Wrap(err, "failed to create database")
		}

		b.database = db
	}

	return b, nil
}

// ServerOption is used to configure a netctl backend.
type ServerOption interface {
	apply(*serverOptions)
}

type serverOptions struct {
	// Database path, if database is enabled.
	databasePath string

	// Do not create the database if this is set.
	withoutDatabase bool

	// Use DBus for UI proxy.
	withDBusProxies bool

	// A URI to listen on for local clients, i.e. netctl CLI
	localListenAddr string
}

type funcServerOption struct {
	f func(*serverOptions)
}

func (fso *funcServerOption) apply(s *serverOptions) {
	fso.f(s)
}

func newFuncServerOption(f func(*serverOptions)) *funcServerOption {
	return &funcServerOption{f}
}

// WithNoDatabase tells the backend to start without a database.
func WithNoDatabase() ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.withoutDatabase = true
	})
}

// WithDatabasePath specifies the database path to use. If this is not
// specified, a default path is used.
func WithDatabasePath(path string) ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.databasePath = path
	})
}

// WithDBusProxies tells the backend to use DBus for UI proxies.
func WithDBusProxies() ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.withDBusProxies = true
	})
}

// WithLocalListener tells the backend to listen on a local address
// to provide a convenient default for local clients like the netctl
// CLI.
func WithLocalListener(addr string) ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.localListenAddr = addr
	})
}

// Serve starts the netctl back service.
func (b *Backend) Serve() error {
	return b.backend.Serve(func(s *grpc.Server) {
		api.RegisterNetctlBackServer(s, b)
	})
}

// Close tears down the Backend.
func (b *Backend) Close() error {
	b.teardownUIProxies()

	return b.backend.Close()
}

func (b *Backend) setRegisterHandler() {
	// Specify the actions to be performed when a frontend registers
	b.backend.SetRegisterHandler(func(nf *metadata.FrontendMetaData) {
		b.initializeUIProxy(nf)

		// Initialize the DB for this frontend.
		if !b.withoutDatabase {
			if err := b.createFrontendPath(nf.Uuid); err != nil {
				log.Printf("Failed to initialize db for %s: %v", nf.Uuid, err)
			}
		}
	})
}

func (b *Backend) setUnregisterHandler() {
	// Specify the actions to be performed when a frontend unregisters
	b.backend.SetUnregisterHandler(func(nf *metadata.FrontendMetaData) {
		if err := b.teardownUIProxy(nf); err != nil {
			log.Printf("Failed to teardown UI proxy for %s: %v", nf.Uuid, err)
		}
	})
}

func (b *Backend) initializeUIProxy(nf *metadata.FrontendMetaData) {
	if !b.withDBusProxies {
		return
	}

	b.addUIProxy(nf)

	go func() {
		err := b.serveUIProxy(nf)
		if err != nil {
			log.Printf("Failed to serve UI proxy: %v", err)
		}
	}()
}

func (b *Backend) serveUIProxy(nf *metadata.FrontendMetaData) error {
	nfc, err := createFrontendClient(nf)
	if err != nil {
		return err
	}

	proxy, err := b.getUIProxy(nf.Uuid)
	if err != nil {
		return err
	}

	return proxy.Serve(nfc)
}

func createFrontendClient(nf *metadata.FrontendMetaData) (*api.FrontendClient, error) {
	ti := &split.Transport{Info: nf.TransportInfo}

	if ti.String() == "" {
		return nil, errors.New("transport unspecified")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	fc := api.NewFrontendClient(cc)

	return fc, nil
}

func (b *Backend) addUIProxy(nf *metadata.FrontendMetaData) {
	b.mux.Lock()
	defer b.mux.Unlock()

	if b.withDBusProxies {
		proxy, err := ui.NewDBusProxy(api.InterfaceType(api.InterfaceType_value[nf.Type]))
		if err != nil {
			log.Printf("Failed to create UI proxy: %v", err)

			return
		}

		b.uiProxies[nf.Uuid] = proxy
	}
}

func (b *Backend) getUIProxy(uuid string) (ui.Proxy, error) {
	b.mux.Lock()
	defer b.mux.Unlock()

	proxy, ok := b.uiProxies[uuid]
	if !ok {
		return nil, errors.New("UI proxy does not exist")
	}

	return proxy, nil
}

func (b *Backend) removeUIProxy(uuid string) {
	b.mux.Lock()
	defer b.mux.Unlock()

	delete(b.uiProxies, uuid)
}

func (b *Backend) teardownUIProxy(nf *metadata.FrontendMetaData) error {
	if !b.withDBusProxies {
		return nil
	}

	proxy, err := b.getUIProxy(nf.Uuid)
	if err != nil {
		return err
	}

	proxy.Close()
	b.removeUIProxy(nf.Uuid)

	return nil
}

func (b *Backend) teardownUIProxies() {
	if !b.withDBusProxies {
		return
	}

	b.mux.Lock()
	defer b.mux.Unlock()

	for uuid, proxy := range b.uiProxies {
		proxy.Close()

		delete(b.uiProxies, uuid)
	}
}

// SaveNetwork caches a network provided by a frontend.
func (b *Backend) SaveNetwork(ctx context.Context, r *api.SaveNetworkRequest) (*api.SaveNetworkReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	err := b.saveNetwork(uuid, r.GetNetwork())
	if err != nil {
		return nil, errors.Wrap(err, "failed to write network configuration")
	}

	return &api.SaveNetworkReply{}, nil
}

// GetSavedNetworks returns a list of cached network for a given frontend..
func (b *Backend) GetSavedNetworks(ctx context.Context, r *api.GetSavedNetworksRequest) (*api.GetSavedNetworksReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	networks, err := b.getSavedNetworks(uuid)
	if err != nil {
		return nil, err
	}

	return &api.GetSavedNetworksReply{Networks: networks}, nil
}

// ForgetNetwork deletes a network from the list of saved networks for a given frontend.
func (b *Backend) ForgetNetwork(ctx context.Context, r *api.ForgetNetworkRequest) (*api.ForgetNetworkReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid, network := r.GetFrontend().GetUuid(), r.GetNetwork()
	if uuid == "" || network == nil {
		return nil, errors.New("must specify uuid and network configuration")
	}

	if err := b.removeNetwork(uuid, network); err != nil {
		return nil, err
	}

	return &api.ForgetNetworkReply{}, nil
}

// ForgetAllNetworks deletes all saved networks for a given frontend.
func (b *Backend) ForgetAllNetworks(ctx context.Context, r *api.ForgetAllNetworksRequest) (*api.ForgetAllNetworksReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()
	if uuid == "" {
		return nil, errors.New("must specify uuid")
	}

	networks, err := b.getSavedNetworks(uuid)
	if err != nil {
		return nil, err
	}

	for _, network := range networks {
		if err := b.removeNetwork(uuid, network); err != nil {
			return nil, err
		}
	}

	return &api.ForgetAllNetworksReply{}, nil
}

// SaveBearerConf caches a bearer config provided by a frontend.
func (b *Backend) SaveBearerConf(ctx context.Context, r *api.SaveBearerConfRequest) (*api.SaveBearerConfReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	err := b.saveBearerConf(uuid, r.GetBearerCfg())
	if err != nil {
		return nil, errors.Wrap(err, "failed to write bearer configuration")
	}

	return &api.SaveBearerConfReply{}, nil
}

// GetSavedBearerConfs returns a list of cached bearer configs for a given frontend.
func (b *Backend) GetSavedBearerConfs(ctx context.Context, r *api.GetSavedBearerConfsRequest) (*api.GetSavedBearerConfsReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	confs, err := b.getSavedBearerConfs(uuid)
	if err != nil {
		return nil, err
	}

	return &api.GetSavedBearerConfsReply{BearerCfgs: confs}, nil
}

// SaveWired8021XConf caches a wired 802.1x config provided by a frontend.
func (b *Backend) SaveWired8021XConf(ctx context.Context, r *api.SaveWired8021XConfRequest) (*api.SaveWired8021XConfReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	err := b.saveWired8021xConf(uuid, r.GetConfig())
	if err != nil {
		return nil, errors.Wrap(err, "failed to write bearer configuration")
	}

	return &api.SaveWired8021XConfReply{}, nil
}

// GetSavedWired8021XConfs returns a list of cached wired 802.1x configs for a given frontend.
func (b *Backend) GetSavedWired8021XConfs(ctx context.Context, r *api.GetSavedWired8021XConfsRequest) (*api.GetSavedWired8021XConfsReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	confs, err := b.getSavedWired8021xConfs(uuid)
	if err != nil {
		return nil, err
	}

	return &api.GetSavedWired8021XConfsReply{Configs: confs}, nil
}
