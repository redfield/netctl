// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split"
	"gitlab.com/redfield/split/metadata"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wired"
)

// WiredFrontend is a netctl frontend that represents a wired interface.
type WiredFrontend struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedNetctlFrontServer

	frontend *split.Frontend
	bc       *api.BackendClient

	manager *wired.Manager
}

// NewWiredFrontend returns a new netctl frontend for a wired interface.
func NewWiredFrontend(ifname, addr string) (*WiredFrontend, error) {
	manager, err := wired.NewManager(ifname)
	if err != nil {
		return nil, err
	}

	frontend, err := split.NewFrontend(addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create frontend")
	}

	wf := &WiredFrontend{
		frontend: frontend,
		manager:  manager,
	}

	uuid, err := hashInterfaceNameWithUUID(ifname)
	if err != nil {
		uuid = ifname
	}

	wf.frontend.Metadata.Uuid = uuid
	wf.frontend.Metadata.Type = api.InterfaceType_WIRED.String()

	return wf, nil
}

// Initialize starts the initialization phase and performs startup actions.
func (wf *WiredFrontend) Initialize(nb *metadata.BackendMetaData, timeout time.Duration) error {
	err := wf.frontend.Initialize(nb, timeout)
	if err != nil {
		return err
	}

	t := &split.Transport{
		Info: nb.TransportInfo,
	}

	cc, err := t.ClientConn()
	if err != nil {
		return errors.Wrap(err, "failed to create backend client after initialization")
	}
	wf.bc = api.NewBackendClient(cc)

	return nil
}

// Serve starts the wired frontend service.
func (wf *WiredFrontend) Serve() error {
	return wf.frontend.Serve(func(s *grpc.Server) {
		api.RegisterNetctlFrontServer(s, wf)
	})
}

// Close tears down the WiredFrontend
func (wf *WiredFrontend) Close() error {
	_ = wf.manager.Close()

	return wf.frontend.Close()
}

func (wf *WiredFrontend) WiredGetProperties(ctx context.Context, r *api.WiredGetPropertiesRequest) (*api.WiredGetPropertiesReply, error) {
	props := &api.WiredProperties{
		State:     convertWiredState(wf.manager.State()),
		IpAddress: wf.manager.IPAddress(),
		IfaceName: wf.manager.IfaceName(),
	}

	return &api.WiredGetPropertiesReply{Props: props}, nil
}

func (wf *WiredFrontend) WiredAuthenticate8021X(ctx context.Context, r *api.WiredAuthenticate8021XRequest) (*api.WiredAuthenticate8021XReply, error) {
	conf := r.GetConfig()
	if conf == nil {
		return nil, errors.New("received empty configuration")
	}

	if r.GetTrySavedConfiguration() {
		return wf.tryAuthenticateWithSaved(conf)
	}

	state, err := wf.handleAuthenticate(conf)
	if err != nil {
		return nil, err
	}

	return &api.WiredAuthenticate8021XReply{State: state}, nil
}

func (wf *WiredFrontend) handleAuthenticate(conf *api.Wired8021XConfiguration) (api.WiredState, error) {
	state, err := wf.manager.Authenticate8021x(formatConfiguration8021x(conf))
	if err != nil {
		return api.WiredState_WIRED_UNKNOWN, errors.Wrap(err, "failed to authenticate")
	}

	switch state {
	case wired.Connected:
		wf.save8021xConfiguration(conf)

		return api.WiredState_WIRED_CONNECTED, nil
	case wired.Failed:
		return api.WiredState_WIRED_FAILED, nil
	default:
		return api.WiredState_WIRED_DISCONNECTED, nil
	}
}

func (wf *WiredFrontend) tryAuthenticateWithSaved(conf *api.Wired8021XConfiguration) (*api.WiredAuthenticate8021XReply, error) {
	nf := wf.frontend.Metadata

	confs, err := wf.bc.GetSavedWired8021XConfs(nf)
	if err != nil {
		// This sort of error will really not be helpful to
		// a client. Instead, we should say "no config found"
		// so that manual input can take over.
		return &api.WiredAuthenticate8021XReply{NoSavedConfiguration: true}, nil
	}

	var savedConf *api.Wired8021XConfiguration

	for _, c := range confs {
		if c.Name == conf.Name {
			savedConf = c
			break
		}
	}

	if savedConf == nil {
		return &api.WiredAuthenticate8021XReply{NoSavedConfiguration: true}, nil
	}

	state, err := wf.handleAuthenticate(savedConf)
	if err != nil {
		return nil, err
	}

	return &api.WiredAuthenticate8021XReply{State: state}, nil
}

func (wf *WiredFrontend) save8021xConfiguration(conf *api.Wired8021XConfiguration) {
	if conf.Name == "" {
		conf.Name = uuid.New().String()
	}

	err := wf.bc.SaveWired8021XConf(wf.frontend.Metadata, conf)
	if err != nil {
		log.Printf("Failed to save 802.1x configuration for %q", conf.Name)
	}
}
