// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split"
	"gitlab.com/redfield/split/metadata"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

// WirelessFrontend is a netctl frontend that is linked to a wireless interface.
type WirelessFrontend struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedNetctlFrontServer

	frontend *split.Frontend

	bc *api.BackendClient

	ctx    context.Context
	cancel context.CancelFunc

	// Wireless interface name and manager
	ifname  string
	manager *wireless.Manager

	// Properties and configuration, protected by lock.
	mux  sync.Mutex
	conf *api.WirelessNetworkConfiguration

	// Wireless options, e.g. periodic scan, networking caching, etc.
	*wirelessOptions
}

// NewWirelessFrontend returns a new netctl frontend for a wireless interface.
func NewWirelessFrontend(ifname, addr string, opts ...WirelessOption) (*WirelessFrontend, error) {
	frontend, err := split.NewFrontend(addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create frontend")
	}

	ctx, cancel := context.WithCancel(context.Background())

	// Default the uuid to be the interface name.
	// This can be overridden using the WithUUID
	// option.
	frontend.Metadata.Uuid = ifname
	frontend.Metadata.Type = api.InterfaceType_WIRELESS.String()

	wf := &WirelessFrontend{
		frontend:        frontend,
		ifname:          ifname,
		conf:            &api.WirelessNetworkConfiguration{},
		wirelessOptions: &wirelessOptions{},
		ctx:             ctx,
		cancel:          cancel,
	}

	for _, opt := range opts {
		opt.apply(wf.wirelessOptions)
	}

	manager, err := wireless.NewManager(ifname, wf.managerOpts...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create wireless manager")
	}
	wf.manager = manager

	if wf.doAssignUUID {
		uuid, err := hashInterfaceNameWithUUID(ifname)
		if err != nil {
			return nil, errors.Wrap(err, "failed to generate uuid for frontend")
		}
		wf.frontend.Metadata.Uuid = uuid
	}

	return wf, nil
}

// Initialize starts the initialization phase and performs startup actions.
func (wf *WirelessFrontend) Initialize(nb *metadata.BackendMetaData, timeout time.Duration) error {
	err := wf.frontend.Initialize(nb, timeout)
	if err != nil {
		return err
	}

	t := &split.Transport{
		Info: nb.TransportInfo,
	}

	cc, err := t.ClientConn()
	if err != nil {
		return errors.Wrap(err, "failed to create backend client after initialization")
	}
	wf.bc = api.NewBackendClient(cc)

	wf.postInitialization()

	return nil
}

// Serve starts the wireless frontend service.
func (wf *WirelessFrontend) Serve() error {
	return wf.frontend.Serve(func(s *grpc.Server) {
		api.RegisterNetctlFrontServer(s, wf)
	})
}

// Close tears down the WirelessFrontend
func (wf *WirelessFrontend) Close() error {
	if err := wf.manager.Close(); err != nil {
		return errors.Wrap(err, "failed to close wireless manager")
	}

	if wf.bc != nil {
		if err := wf.bc.Close(); err != nil {
			return errors.Wrap(err, "failed to close backend client")
		}
	}

	return wf.frontend.Close()
}

// postInitialization performs any actions that should happen as
// a part of service startup, but need to wait until the frontend
// has initialized with the backend.
func (wf *WirelessFrontend) postInitialization() {
	go wf.manager.ScanForNetworks(wf.ctx)
	go wf.connectToLastNetwork()
}

// setConf sets the frontends network configuration.
func (wf *WirelessFrontend) setConf(conf *api.WirelessNetworkConfiguration) {
	wf.mux.Lock()
	defer wf.mux.Unlock()

	wf.conf = conf
}

// WirelessOption is used to configure a netctl frontend.
type WirelessOption interface {
	apply(*wirelessOptions)
}

type wirelessOptions struct {
	// Indicates if the wireless frontend should try to connect to
	// most-recently used known network on startup. If there are
	// none in present in the scan results, nothing happens.
	doConnectLastNetwork bool

	// Indicates if the frontend should cache network configurations,
	// and re-use them on connect.
	doRememberNetworks bool

	// Indicates that a proper UUID should be assigned to the frontend,
	// rather than indentifying it by its interface name.
	doAssignUUID bool

	// Options to use when creating the wireless.Manager.
	managerOpts []wireless.ManagerOption
}

type funcWirelessOption struct {
	f func(*wirelessOptions)
}

func (fwo *funcWirelessOption) apply(w *wirelessOptions) {
	fwo.f(w)
}

func newFuncWirelessOption(f func(*wirelessOptions)) *funcWirelessOption {
	return &funcWirelessOption{f}
}

// WithConnectLastNetwork tells the Service to try to connect to the last
// known network on startup.
func WithConnectLastNetwork() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doConnectLastNetwork = true
	})
}

// WithRememberNetworks tells the Service to cache network configurations.
func WithRememberNetworks() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doRememberNetworks = true
	})
}

// WithUUID tells the frontend to use a UUID as its identifier with the backend.
// By default, the frontend is identified by its interface name.
func WithUUID() WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.doAssignUUID = true
	})
}

// WithManagerOptions specifies a list of wireless.ManagerOptions that the
// frontend should used when constructing its wireless.Manager.
func WithManagerOptions(mopts ...wireless.ManagerOption) WirelessOption {
	return newFuncWirelessOption(func(w *wirelessOptions) {
		w.managerOpts = mopts
	})
}

func (wf *WirelessFrontend) connectToLastNetwork() {
	if !wf.doConnectLastNetwork {
		return
	}

	nf := wf.frontend.Metadata

	networks, err := wf.bc.GetSavedNetworks(nf)
	if err != nil {
		log.Print(errors.Wrap(err, "failed to get saved networks"))
		return
	}

	if len(networks) == 0 {
		// Nothing to do
		return
	}

	formatted := make([]wireless.NetworkConfiguration, 0)
	for _, n := range networks {
		// The user has asked netctl not to auto connect
		// if this network is found.
		if n.NoAutoConnect {
			continue
		}

		formatted = append(formatted, formatNetworkConfiguration(n))
	}

	err = wf.manager.SearchAndConnect(wf.ctx, formatted...)
	if err != nil {
		log.Printf("Failed to connect to known network: %v", err)
	}
}

// WirelessConnect connects to a wireless network.
func (wf *WirelessFrontend) WirelessConnect(ctx context.Context, r *api.WirelessConnectRequest) (*api.WirelessConnectReply, error) {
	conf := r.GetConfig()
	if conf == nil {
		return nil, errors.New("received empty configuration")
	}

	if r.GetTrySavedConfiguration() {
		return wf.tryConnectWithSaved(conf)
	}

	s, err := wf.handleConnect(conf)
	if err != nil {
		return nil, err
	}

	return &api.WirelessConnectReply{State: s}, nil
}

func (wf *WirelessFrontend) tryConnectWithSaved(conf *api.WirelessNetworkConfiguration) (*api.WirelessConnectReply, error) {
	nf := wf.frontend.Metadata

	networks, err := wf.bc.GetSavedNetworks(nf)
	if err != nil {
		// This sort of error will really not be helpful to
		// a client. Instead, we should say "no config found"
		// so that manual input can take over.
		return &api.WirelessConnectReply{NoSavedConfiguration: true}, nil
	}

	var savedConf *api.WirelessNetworkConfiguration

	for _, n := range networks {
		if n.Ssid == conf.Ssid {
			savedConf = n
			break
		}
	}

	if savedConf == nil {
		return &api.WirelessConnectReply{NoSavedConfiguration: true}, nil
	}

	// It is possible the user preference for "connect automatically"
	// has changed here. To be sure, update the saved configuration
	// with what ever was given.
	savedConf.NoAutoConnect = conf.NoAutoConnect

	s, err := wf.handleConnect(savedConf)
	if err != nil {
		return nil, err
	}

	return &api.WirelessConnectReply{State: s}, nil
}

func (wf *WirelessFrontend) handleConnect(conf *api.WirelessNetworkConfiguration) (api.WirelessState, error) {
	state, err := wf.manager.Connect(formatNetworkConfiguration(conf))
	if err != nil {
		return api.WirelessState_UNKNOWN, errors.Wrap(err, "wireless manager failed connection attempt")
	}

	switch state {
	case wireless.Connected:
		wf.saveNetworkConfiguration(conf)

		return api.WirelessState_CONNECTED, nil
	case wireless.Failed:
		return api.WirelessState_FAILED, nil
	default:
		return api.WirelessState_DISCONNECTED, nil
	}
}

func (wf *WirelessFrontend) saveNetworkConfiguration(conf *api.WirelessNetworkConfiguration) {
	if wf.doRememberNetworks {
		err := wf.bc.SaveNetwork(wf.frontend.Metadata, conf)
		if err != nil {
			log.Printf("Failed to save network configuration for '%v'", conf.Ssid)
		}
	}

	wf.setConf(conf)
}

// WirelessDisconnect disconnects a wireless network.
func (wf *WirelessFrontend) WirelessDisconnect(ctx context.Context, r *api.WirelessDisconnectRequest) (*api.WirelessDisconnectReply, error) {
	err := wf.handleDisconnect()
	if err != nil {
		return nil, err
	}

	return &api.WirelessDisconnectReply{}, nil
}

func (wf *WirelessFrontend) handleDisconnect() error {
	err := wf.manager.Disconnect()
	if err != nil {
		return errors.Wrap(err, "wireless manager failed disconnect")
	}

	wf.setConf(&api.WirelessNetworkConfiguration{})

	return nil
}

// InterfaceEnable enables the network interface controlled by the frontend.
func (wf *WirelessFrontend) EnableInterface(ctx context.Context, r *api.EnableInterfaceRequest) (*api.EnableInterfaceReply, error) {
	err := wf.manager.EnableInterface()
	if err != nil {
		return nil, err
	}
	return &api.EnableInterfaceReply{}, nil
}

// InterfaceDisable disables the network interface controlled by the frontend.
func (wf *WirelessFrontend) DisableInterface(ctx context.Context, r *api.DisableInterfaceRequest) (*api.DisableInterfaceReply, error) {
	err := wf.manager.DisableInterface()
	if err != nil {
		return nil, err
	}

	return &api.DisableInterfaceReply{}, nil
}

// WirelessMonitorProperties handles a stream of wireless properties updates.
func (wf *WirelessFrontend) WirelessMonitorProperties(r *api.WirelessMonitorPropertiesRequest, stream api.NetctlFront_WirelessMonitorPropertiesServer) error {
	ctx, cancel := context.WithCancel(wf.ctx)
	defer cancel()

	updates := make(chan *api.WirelessPropertiesUpdate, 16)

	go wf.doMonitorProperties(ctx, updates)

	for {
		var u *api.WirelessPropertiesUpdate

		select {
		case <-wf.ctx.Done():
			return errors.New("frontend closing")
		case u = <-updates:
		}

		r := &api.WirelessMonitorPropertiesReply{Update: u}

		if err := stream.Send(r); err != nil {
			return errors.Wrap(err, "failed to send wireless properties update")
		}
	}
}

func (wf *WirelessFrontend) doMonitorProperties(ctx context.Context, update chan *api.WirelessPropertiesUpdate) {
	// Keep a local copy of properties. The only job of this function
	// is to relay information from the wireless.Manager to the RPC
	// caller.
	props := wf.getWirelessProperties()

	c := make(chan wireless.Notification, 16)
	wf.manager.Notify(ctx, c)

	for {
		var (
			n     wireless.Notification
			utype api.WirelessPropertiesUpdate_Type
		)

		select {
		case <-ctx.Done():
			return
		case n = <-c:
		}

		switch n.Type {
		case wireless.StateChanged:
			utype = api.WirelessPropertiesUpdate_STATE
			props.State = convertWirelessState(n.Value.(wireless.State))
			props.PortalUrl = "" // Make sure portal url is cleared.

		case wireless.ScanResultsChanged:
			utype = api.WirelessPropertiesUpdate_ACCESSPOINTS
			props.AccessPoints = convertWirelessScanResults(n.Value.([]wireless.ScanResult))

		case wireless.SignalStrengthChanged:
			utype = api.WirelessPropertiesUpdate_SIGNAL
			props.SignalStrength = n.Value.(wireless.SignalStrength).String()

		case wireless.SSIDChanged:
			utype = api.WirelessPropertiesUpdate_SSID
			props.Ssid = n.Value.(string)

		case wireless.IPChanged:
			utype = api.WirelessPropertiesUpdate_IP
			props.IpAddress = n.Value.(string)

			// Clients written for an older version might expect
			// the SSID to be set when the IP address changes.
			props.Ssid = wf.manager.CurrentSSID()

		case wireless.PortalDetected:
			utype = api.WirelessPropertiesUpdate_STATE
			props.State = api.WirelessState_PORTAL
			props.PortalUrl = n.Value.(string)

		case wireless.PortalResolved:
			utype = api.WirelessPropertiesUpdate_STATE
			props.State = api.WirelessState_CONNECTED
			props.PortalUrl = ""
		}

		// Relay the update.
		update <- &api.WirelessPropertiesUpdate{Type: utype, Props: props}
	}
}

// WirelessGetProperties returns the current properties of the wireless frontend.
func (wf *WirelessFrontend) WirelessGetProperties(ctx context.Context, r *api.WirelessGetPropertiesRequest) (*api.WirelessGetPropertiesReply, error) {
	props := wf.getWirelessProperties()

	return &api.WirelessGetPropertiesReply{Props: props}, nil
}

func (wf *WirelessFrontend) getWirelessProperties() *api.WirelessProperties {
	props := &api.WirelessProperties{
		State:          convertWirelessState(wf.manager.State()),
		Ssid:           wf.manager.CurrentSSID(),
		IfaceName:      wf.manager.IfaceName(),
		IpAddress:      wf.manager.IPAddress(),
		SignalStrength: wf.manager.SignalStrength().String(),
		AccessPoints:   convertWirelessScanResults(wf.manager.AvailableNetworks()),
	}

	// If we can get a reliable portal check, use that information
	// to update the props. Otherwise, leave it as-is.
	pres, url, err := wf.manager.CaptivePortalCheck()
	if err == nil && pres {
		props.State = api.WirelessState_PORTAL
		props.PortalUrl = url
	}

	return props
}
