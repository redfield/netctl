// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wired

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
)

// OperState represents the operational state of the wired
// interface as per RFC2863.
type OperState int32

const (
	// OperStateInvalid represents an OperState that is not defined.
	OperStateInvalid OperState = iota
	OperStateUnknown
	OperStateDown
	OperStateUp
	OperStateNotPresent
	OperStateLowerLayerDown
	OperStateTesting
	OperStateDormant
)

// String returns the string representation of OperState in the same manner as
// the Linux sysfs.If OperState is not a defined constant, String returns
// "<invalid>".
func (o OperState) String() string {
	switch o {
	case OperStateUnknown:
		return "unknown"
	case OperStateDown:
		return "down"
	case OperStateUp:
		return "up"
	case OperStateNotPresent:
		return "notpresent"
	case OperStateLowerLayerDown:
		return "lowerlayerdown"
	case OperStateTesting:
		return "testing"
	case OperStateDormant:
		return "dormant"
	default:
		return "<invalid>"
	}
}

// State represents a connection state when the operational state is
// "up", i.e. OperStateUp.
type State int32

// Possible values of type State:
// 	Disconnected
// 	Connected
// 	Connecting
// 	Failed
const (
	Disconnected State = iota
	Connected
	Connecting
	Failed
)

// String returns the string represetation of the state.
func (s State) String() string {
	switch s {
	case Disconnected:
		return "disconnected"
	case Connected:
		return "connected"
	case Connecting:
		return "connecting"
	case Failed:
		return "failed"
	}

	return "unknown"
}

// Manager is used to manage a wired network interface.
type Manager struct {
	iface *net.Interface

	// The operational state of the interface, locked
	// by mutex because it is updated by a poller
	// in another go routine.
	mu   sync.RWMutex
	oper OperState

	ctx    context.Context
	cancel context.CancelFunc

	// supplicant is used when a wired frontend needs
	// 802.1x authentication.
	supplicant *supplicant
}

// NewManager returns a Manager for a specified wired interface.
func NewManager(ifname string) (*Manager, error) {
	iface, err := net.InterfaceByName(ifname)
	if err != nil {
		return nil, err
	}

	supplicant, err := newSupplicant(ifname)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize supplicant")
	}

	ctx, cancel := context.WithCancel(context.Background())

	m := &Manager{
		iface:      iface,
		ctx:        ctx,
		cancel:     cancel,
		supplicant: supplicant,
	}

	go m.pollOperState()

	return m, nil
}

func (m *Manager) Close() error {
	if m.cancel != nil {
		m.cancel()
		m.cancel = nil
	}

	if m.supplicant != nil {
		return m.supplicant.Close()
	}

	return nil
}

// IfaceName returns the name of the network interface managed by a Manager
func (m *Manager) IfaceName() string {
	return m.iface.Name
}

// State returns the current state of the network interface managed by manager.
func (m *Manager) State() State {
	// Only consider non-operational state if the interface is UP.
	if m.OperState() != OperStateUp {
		return Disconnected
	}

	// If we are UP, and have an IP, call it good.
	if m.IPAddress() != "" {
		return Connected
	}

	// If we get here, it is likely because this interface is on an 802.1x
	// authenticated port. Look at wpa_supplicant state.
	switch state := m.supplicant.getState(); state {
	case wpaStateDisconnected, wpaStateInactive:
		return Disconnected

	case wpaStateAssociated:
		return Connecting

	case wpaStateCompleted:
		// Probably waiting on DHCP...
		return Connecting

	default:
		log.Printf("Unknown supplicant state %q", state)
		return Disconnected
	}
}

// IPAddress returns a dotted decimal string representation of
// the Interface's IP address. If it has no IP, an empty string
// is returned.
func (m *Manager) IPAddress() string {
	ip, err := m.ip4Addr()
	if err != nil {
		return ""
	}

	return ip.String()
}

func (m *Manager) ip4Addr() (net.IP, error) {
	addrs, err := m.iface.Addrs()
	if err != nil {
		return net.IP{}, err
	}

	// Look for valid IP address
	for _, v := range addrs {
		// Make sure the address is valid IPv4 in dotted decimal notation
		addr := strings.Split(v.String(), "/")[0]
		ip := net.ParseIP(addr).To4()

		if ip != nil {
			// Found a valid IPv4 address
			return ip, nil
		}
	}

	// Interface has no addresses
	return net.IP{}, fmt.Errorf("%v has no valid IPv4 address", m.iface.Name)
}

func (m *Manager) OperState() OperState {
	m.mu.RLock()
	defer m.mu.RUnlock()

	return m.oper
}

func operStateFromRaw(data []byte) OperState {
	s := strings.TrimSpace(string(data))
	s = strings.ToLower(s)

	var state OperState

	switch s {
	case (OperStateUnknown).String():
		state = OperStateUnknown

	case (OperStateDown).String():
		state = OperStateDown

	case (OperStateUp).String():
		state = OperStateUp

	case (OperStateNotPresent).String():
		state = OperStateNotPresent

	case (OperStateLowerLayerDown).String():
		state = OperStateLowerLayerDown

	case (OperStateTesting).String():
		state = OperStateTesting

	case (OperStateDormant).String():
		state = OperStateDormant

	default:
		state = OperStateInvalid
	}

	return state
}

func (m *Manager) pollOperState() {
	path := fmt.Sprintf("/sys/class/net/%v/operstate", m.IfaceName())

	for {
		select {
		case <-m.ctx.Done():
			return
		case <-time.After(time.Second):
		}

		data, err := ioutil.ReadFile(path)
		if err != nil {
			log.Printf("Error: reading %v: %v", path, err)
			continue
		}

		oper := operStateFromRaw(data)
		if oper != m.OperState() {
			m.mu.Lock()
			m.oper = oper
			m.mu.Unlock()

			m.handleOperStateChange(oper)
		}
	}
}

func (m *Manager) handleOperStateChange(oper OperState) {
	ifname := m.IfaceName()

	log.Printf("%s: operstate = %s", ifname, oper)

	switch oper {
	case OperStateUp:
		err := m.supplicant.reauthenticate()
		if err != nil {
			if err == errNoConfiguredNetwork {
				return
			}

			log.Printf("%s: re-authentication failed: %v", ifname, err)
		} else {
			log.Printf("%s: re-authentication succeeded", ifname)
		}
	case OperStateDown:
		err := m.supplicant.disableCurrentNetwork()
		if err != nil {
			if err == errNoConfiguredNetwork {
				return
			}

			log.Printf("%s: failed to disable network: %v", ifname, err)
		} else {
			log.Printf("%s: disabled network", ifname)
		}
	default:
		log.Printf("%s: nothing to do for operstate %s", ifname, oper)
	}
}

func (m *Manager) Authenticate8021x(cfg Configuration8021x) (State, error) {
	_ = m.supplicant.removeConfiguredNetwork()

	success, err := m.supplicant.authenticate(cfg)
	if !success {
		_ = m.supplicant.removeConfiguredNetwork()

		return Failed, err
	}

	return Connected, nil
}
