// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wired

import (
	"context"
	"encoding/pem"
	"fmt"
	"log"
	"strings"
	"sync"

	"github.com/godbus/dbus/v5"
	"github.com/pkg/errors"
)

const (
	// DBus destination and base object path for wpa_supplicant.
	wpaDBusDestination = "fi.w1.wpa_supplicant1"
	wpaBaseObjectPath  = "/fi/w1/wpa_supplicant1"
)

// wpa_supplicant interface states
const (
	wpaStateDisconnected = "disconnected"
	wpaStateInactive     = "inactive"
	wpaStateAssociated   = "associated"
	wpaStateCompleted    = "completed"
)

// Configuration8021x represents authentication information for a
// wired network with 802.1x.
type Configuration8021x struct {
	// Name is used to identify the configuration.
	Name string

	// 802.1x configuration field.
	EAP           string
	KeyManagement string
	Identity      string
	Password      string
	Phase1        string
	Phase2        string
	CaCert        []byte
	ClientCert    []byte
	PrivKey       []byte
	PrivKeyPasswd string
}

func (s *supplicant) configuration8021xAsMap(c Configuration8021x) (map[string]interface{}, error) {
	const (
		cfgKeyManagement = "key_mgmt"
		cfgIdentity      = "identity"
		cfgPassword      = "password"
		cfgEAP           = "eap"
		cfgCaCert        = "ca_cert"
		cfgClientCert    = "client_cert"
		cfgPrivKey       = "private_key"
		cfgPrivKeyPasswd = "private_key_passwd"
		cfgPhase1        = "phase1"
		cfgPhase2        = "phase2"
	)

	m := make(map[string]interface{})

	if c.KeyManagement != "" {
		m[cfgKeyManagement] = c.KeyManagement
	}

	if c.Identity != "" {
		m[cfgIdentity] = c.Identity
	}

	if c.Password != "" {
		m[cfgPassword] = c.Password
	}

	if c.EAP != "" {
		m[cfgEAP] = c.EAP
	}

	if c.Phase1 != "" {
		m[cfgPhase1] = c.Phase1
	}

	if c.Phase2 != "" {
		m[cfgPhase2] = c.Phase2
	}

	if c.PrivKeyPasswd != "" {
		m[cfgPrivKeyPasswd] = c.PrivKeyPasswd
	}

	// Raw byte values. These configuration options are used
	// as "configuration blobs", i.e. named binary blobs.
	// So, for each of these, we must import a blob and then
	// set the map value to "blob://<Name>_<cfgKey>".
	//
	// See: https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf
	if c.ClientCert != nil {
		name := fmt.Sprintf("%s_%s", c.Name, cfgClientCert)

		block, _ := pem.Decode(c.ClientCert)
		if block.Bytes == nil {
			return nil, errors.New("client certificate must be PEM encoded")
		}

		_ = s.removeBlob(name)
		err := s.addBlob(name, block.Bytes)
		if err != nil {
			return nil, err
		}

		m[cfgClientCert] = fmt.Sprintf("blob://%s", name)
	}

	if c.CaCert != nil {
		name := fmt.Sprintf("%s_%s", c.Name, cfgCaCert)

		block, _ := pem.Decode(c.CaCert)
		if block.Bytes == nil {
			return nil, errors.New("client certificate must be PEM encoded")
		}

		_ = s.removeBlob(name)
		err := s.addBlob(name, block.Bytes)
		if err != nil {
			return nil, err
		}

		m[cfgCaCert] = fmt.Sprintf("blob://%s", name)
	}

	if c.PrivKey != nil {
		name := fmt.Sprintf("%s_%s", c.Name, cfgPrivKey)

		// The private key may be a PKCS #12 bundle, or a PEM
		// encoded private key. Try to decode PEM, but if that
		// fails, just pass the data as-is to wpa supplicant.
		// It will read the PKCS #12 bundle itself.
		data := c.PrivKey

		block, _ := pem.Decode(data)
		if block != nil {
			data = block.Bytes
		}

		_ = s.removeBlob(name)
		err := s.addBlob(name, data)
		if err != nil {
			return nil, err
		}

		m[cfgPrivKey] = fmt.Sprintf("blob://%s", name)
	}

	return m, nil
}

var errNoConfiguredNetwork = errors.New("no configured network")

// supplicant is a wrapper around the wpa_supplicant DBus API
// specifically for use with wired interfaces to perform 802.1x
// authentication.
type supplicant struct {
	bus   *dbus.Conn      // DBus connection
	iface dbus.ObjectPath // Object path for network interface

	mu           sync.Mutex
	state        string
	savedNetwork dbus.ObjectPath // Object path for a saved network

	// Use to manage goroutine lifetimes
	ctx    context.Context
	cancel context.CancelFunc
}

func newSupplicant(ifname string) (*supplicant, error) {
	// Connect to system bus
	bus, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	var iface dbus.ObjectPath

	// Create a bus object to the base path.
	obj := bus.Object(wpaDBusDestination, wpaBaseObjectPath)

	args := map[string]interface{}{
		"Ifname": ifname,
		"Driver": "wired",
	}
	err = obj.Call("fi.w1.wpa_supplicant1.CreateInterface", 0, args).Store(&iface)
	if err != nil {
		// We may have gotten an error because this already exists.
		err = obj.Call("fi.w1.wpa_supplicant1.GetInterface", 0, ifname).Store(&iface)
		if err != nil {
			return nil, err
		}
	}

	ctx, cancel := context.WithCancel(context.Background())

	s := supplicant{
		bus:          bus,
		iface:        iface,
		savedNetwork: "/",
		ctx:          ctx,
		cancel:       cancel,
	}

	if err := s.initSignalHandlers(); err != nil {
		return nil, err
	}

	return &s, nil
}

func (s *supplicant) Close() error {
	if s.cancel != nil {
		s.cancel()
		s.cancel = nil
	}

	return nil
}

func (s *supplicant) getState() string {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.state
}

func (s *supplicant) addBlob(key string, data []byte) error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)

	call := obj.Call("fi.w1.wpa_supplicant1.Interface.AddBlob", 0, key, data)

	return call.Err
}

func (s *supplicant) removeBlob(key string) error {
	obj := s.bus.Object(wpaDBusDestination, s.iface)

	call := obj.Call("fi.w1.wpa_supplicant1.Interface.RemoveBlob", 0, key)

	return call.Err
}

func (s *supplicant) initSignalHandlers() error {
	opts := []dbus.MatchOption{
		dbus.WithMatchObjectPath(s.iface),
		dbus.WithMatchInterface("fi.w1.wpa_supplicant1.Interface"),
		dbus.WithMatchSender(wpaDBusDestination),
	}

	err := s.bus.AddMatchSignal(append(opts, dbus.WithMatchMember("PropertiesChanged"))...)
	if err != nil {
		return err
	}

	err = s.bus.AddMatchSignal(append(opts, dbus.WithMatchMember("EAP"))...)
	if err != nil {
		return err
	}

	go s.handlePropertiesChanged()

	return nil
}

func (s *supplicant) handlePropertiesChanged() {
	signal := make(chan *dbus.Signal, 64)
	s.bus.Signal(signal)
	defer s.bus.RemoveSignal(signal)

	for {
		var (
			sig *dbus.Signal
			ok  bool
		)

		select {
		case <-s.ctx.Done():
			return
		case sig, ok = <-signal:
		}

		if !ok {
			log.Print("DBus signal chan closed!")
			return
		}

		if sig.Name != "fi.w1.wpa_supplicant1.Interface.PropertiesChanged" {
			continue
		}

		props, ok := sig.Body[0].(map[string]dbus.Variant)
		if !ok {
			log.Printf("Got unexpected dbus signal body: %+v", sig.Body)
			continue
		}

		if state, ok := props["State"]; ok {
			s.mu.Lock()
			s.state = strings.Trim(state.Value().(string), `"`)
			s.mu.Unlock()
		}
	}
}

type eapResult struct {
	// Indicates that the supplicant successfully interacted
	// with an EAP authenticator, e.g. RADIUS client.
	started bool

	// Indicates that EAP succeeded. This can be vacuously true when
	// no authentication takes place, e.g. the port is open.
	success bool
}

// waitForEAPResult reads EAP signals and waits until the "completion" value
// comes up (one of "success" or "failure"), and writes it over the chan.
func (s *supplicant) waitForEAPResult(ctx context.Context, result chan<- eapResult) {
	signal := make(chan *dbus.Signal, 8)
	s.bus.Signal(signal)

	go func() {
		defer s.bus.RemoveSignal(signal)
		defer close(result)

		var r eapResult

		for {
			var (
				sig *dbus.Signal
				ok  bool
			)

			select {
			case <-ctx.Done():
				return

			case sig, ok = <-signal:
			}

			if !ok {
				return
			}

			if sig.Name != "fi.w1.wpa_supplicant1.Interface.EAP" {
				continue
			}

			// This signal is given as a string array, where the first
			// element is the "status", and the second is the "parameter".
			// We need to look for the "completion" status, and write the
			// parameter over the chan.
			if len(sig.Body) < 2 {
				log.Printf("Unexpected signal body for %s: %v", sig.Name, sig.Body)
				return
			}

			status, ok := sig.Body[0].(string)
			if !ok {
				log.Printf("Expected string at index 0 for %v: got %T", sig.Name, sig.Body[0])
				return
			}

			switch status {
			case "started":
				r.started = true

			case "completion":
				param, ok := sig.Body[1].(string)
				if !ok {
					log.Printf("Expected string at index 1 for %v: got %T", sig.Name, sig.Body[1])
					return
				}

				r.success = param == "success"

				result <- r
				return
			}
		}
	}()
}

func (s *supplicant) authenticate(cfg Configuration8021x) (bool, error) {
	obj := s.bus.Object(wpaDBusDestination, s.iface)

	// Make the call to AddNetwork, and store the result path.
	var path dbus.ObjectPath

	// Make the configuration a map so that it can be used as args
	// to the DBus call.
	args, err := s.configuration8021xAsMap(cfg)
	if err != nil {
		return false, errors.Wrap(err, "failed to parse network configuration")
	}

	err = obj.Call("fi.w1.wpa_supplicant1.Interface.AddNetwork", 0, args).Store(&path)
	if err != nil {
		return false, errors.Wrap(err, "failed to add network configuration")
	}

	// Set up the EAP result handler
	result := make(chan eapResult, 1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	s.waitForEAPResult(ctx, result)

	// Now select the network configuration we just added.
	call := obj.Call("fi.w1.wpa_supplicant1.Interface.SelectNetwork", 0, path)
	if call.Err != nil {
		return false, call.Err
	}

	r := <-result

	// If EAP never started, return an error indicating so. Otherwise, return
	// r.success and no error.
	if !r.started {
		return false, errors.New("no EAP authentication started")
	}

	if r.success {
		s.mu.Lock()
		s.savedNetwork = path
		s.mu.Unlock()
	}

	return r.success, nil
}

// reauthenticate is used to re-auth with a previously configured network,
// which is saved in the savedNetwork field of supplicant. The network
// should still be present in wpa_supplicant, so we just enable it.
func (s *supplicant) reauthenticate() error {
	s.mu.Lock()
	network := s.savedNetwork
	s.mu.Unlock()

	if network == "/" {
		return errNoConfiguredNetwork
	}

	// Set up the EAP result handler
	result := make(chan eapResult, 1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	s.waitForEAPResult(ctx, result)

	if err := s.enableNetwork(network); err != nil {
		return err
	}

	r := <-result

	if !r.started {
		_ = s.disableCurrentNetwork()

		return errors.New("no EAP authentication started")
	}

	if !r.success {
		_ = s.disableCurrentNetwork()

		return errors.New("EAP failed")
	}

	return nil
}

// removeConfiguredNetwork removes (and disconnects) the currently configured
// network. If there is no network, nothing happens and nil error is returned.
func (s *supplicant) removeConfiguredNetwork() error {
	cn, err := s.currentNetwork()
	if err != nil {
		if err == errNoConfiguredNetwork {
			return nil
		}

		return err
	}

	obj := s.bus.Object(wpaDBusDestination, s.iface)

	call := obj.Call("fi.w1.wpa_supplicant1.Interface.RemoveNetwork", 0, cn)

	if call.Err != nil {
		return call.Err
	}

	return nil
}

func (s *supplicant) currentNetwork() (dbus.ObjectPath, error) {
	obj := s.bus.Object(wpaDBusDestination, s.iface)

	v, err := obj.GetProperty("fi.w1.wpa_supplicant1.Interface.CurrentNetwork")
	if err != nil {
		return "", err
	}

	network, ok := v.Value().(dbus.ObjectPath)
	if !ok {
		return "", errors.New("unexpected type for CurrentNetwork")
	}

	if network == "/" {
		return network, errNoConfiguredNetwork
	}

	return network, nil
}

// enableCurrentNetwork writes the Enabled property on the current
// network, if there is one. If there is not a configured network,
// errNoConfiguredNetwork is returned.
func (s *supplicant) enableNetwork(n dbus.ObjectPath) error {
	obj := s.bus.Object(wpaDBusDestination, n)

	return obj.SetProperty("fi.w1.wpa_supplicant1.Network.Enabled", dbus.MakeVariant(true))
}

// disableCurrentNetwork writes the Enabled property on the current
// network, if there is one. If there is not a configured network,
// errNoConfiguredNetwork is returned.
func (s *supplicant) disableCurrentNetwork() error {
	cn, err := s.currentNetwork()
	if err != nil {
		return err
	}

	obj := s.bus.Object(wpaDBusDestination, cn)

	return obj.SetProperty("fi.w1.wpa_supplicant1.Network.Enabled", dbus.MakeVariant(false))
}
