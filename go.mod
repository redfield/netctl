module gitlab.com/redfield/netctl

go 1.11

require (
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/godbus/dbus/v5 v5.0.4
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.1.2
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20190309225732-aa3e7c118975
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/xid v1.2.1
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.4.0
	gitlab.com/redfield/split v0.0.0-20210710092620-2d4c66010a8d
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
	golang.org/x/text v0.3.4 // indirect
	google.golang.org/genproto v0.0.0-20201119123407-9b1e624d6bc4 // indirect
	google.golang.org/grpc v1.34.1
	google.golang.org/protobuf v1.25.0
)
