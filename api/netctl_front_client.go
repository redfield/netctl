// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

// FrontendClient provides a client for the netctl_front service.
type FrontendClient struct {
	c NetctlFrontClient

	// Keep track so it can be closed later.
	cc *grpc.ClientConn

	// Timeout for RPC calls.
	timeout time.Duration
}

// NewFrontendClient returns a new netctl_front client.
func NewFrontendClient(cc *grpc.ClientConn) *FrontendClient {
	fc := &FrontendClient{
		c:       NewNetctlFrontClient(cc),
		cc:      cc,
		timeout: 30 * time.Second,
	}

	return fc
}

// SetTimeout sets the timeout used to call non-streaming RPCs.
func (fc *FrontendClient) SetTimeout(timeout time.Duration) {
	fc.timeout = timeout
}

// Close closes the frontend client connection.
func (fc *FrontendClient) Close() error {
	return fc.cc.Close()
}

// WirelessConnect connects the frontend to a wireless network with a given
// network configuration.
func (fc *FrontendClient) WirelessConnect(conf *WirelessNetworkConfiguration) (WirelessState, error) {
	r := &WirelessConnectRequest{
		Config: conf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WirelessConnect(ctx, r)

	if err != nil {
		return WirelessState_UNKNOWN, err
	}

	return reply.State, nil
}

// WirelessConnectWithSaved trys to connect to the given SSID with a saved
// configuration, rather than supplying all the auth data. WirelessConnectWithSaved
// reports if the network was found in saved configurations. If one was found, and
// the resulting state was not Connected, an error is returned.
func (fc *FrontendClient) WirelessConnectWithSaved(ssid string) (bool, error) {
	conf := &WirelessNetworkConfiguration{
		Ssid: ssid,
	}

	r := &WirelessConnectRequest{
		Config:                conf,
		TrySavedConfiguration: true,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WirelessConnect(ctx, r)
	if err != nil {
		return true, err
	}

	if reply.NoSavedConfiguration {
		return false, nil
	}

	if reply.State != WirelessState_CONNECTED {
		return true, errors.Errorf("saved configuration failed for %s", ssid)
	}

	return true, nil
}

// WirelessDisconnect disconnects the frontend from its wireless network.
func (fc *FrontendClient) WirelessDisconnect() error {
	r := &WirelessDisconnectRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.WirelessDisconnect(ctx, r)

	return err
}

// WirelessPropertiesStream represents a stream of wireless properties.
type WirelessPropertiesStream struct {
	NetctlFront_WirelessMonitorPropertiesClient
}

// WirelessMonitorProperties registers a properties monitor and returns a stream.
func (fc *FrontendClient) WirelessMonitorProperties(ctx context.Context) (*WirelessPropertiesStream, error) {
	r := &WirelessMonitorPropertiesRequest{}

	stream, err := fc.c.WirelessMonitorProperties(ctx, r)
	if err != nil {
		return nil, err
	}

	return &WirelessPropertiesStream{stream}, nil
}

// WirelessGetProperties returns the properties of the wireless frontend.
func (fc *FrontendClient) WirelessGetProperties() (*WirelessProperties, error) {
	r := &WirelessGetPropertiesRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WirelessGetProperties(ctx, r)

	return reply.GetProps(), err
}

func (fc *FrontendClient) DisableInterface() error {
	r := &DisableInterfaceRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.DisableInterface(ctx, r)

	return err
}
func (fc *FrontendClient) EnableInterface() error {
	r := &EnableInterfaceRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.EnableInterface(ctx, r)

	return err
}

func (fc *FrontendClient) ModemConnect(cfg *BearerConfiguration) error {
	r := &ModemConnectRequest{
		BearerCfg: cfg,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.ModemConnect(ctx, r)

	return err
}

func (fc *FrontendClient) ModemDisconnect() error {
	r := &ModemDisconnectRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	_, err := fc.c.ModemDisconnect(ctx, r)

	return err
}

func (fc *FrontendClient) ModemGetProperties() (*ModemProperties, error) {
	r := &ModemGetPropertiesRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.ModemGetProperties(ctx, r)

	return reply.GetProps(), err
}

// WiredGetProperties returns the properties of the wireless frontend.
func (fc *FrontendClient) WiredGetProperties() (*WiredProperties, error) {
	r := &WiredGetPropertiesRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WiredGetProperties(ctx, r)

	return reply.GetProps(), err
}

// WiredAuthenticate8021X attempts to authenticate the wired interface with
// 802.1x.
func (fc *FrontendClient) WiredAuthenticate8021X(conf *Wired8021XConfiguration) (WiredState, error) {
	r := &WiredAuthenticate8021XRequest{
		Config: conf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WiredAuthenticate8021X(ctx, r)

	if err != nil {
		return WiredState_WIRED_UNKNOWN, err
	}

	return reply.State, nil
}

// WiredAuthenticate8021XWithSaved trys to authenticated with a saved
// configuration of a given name, rather than re-supplying the auth data.
// WiredAuthenticate8021XWithSaved reports if a matching config was found in
// saved configurations. If one was found, and the resulting state was not
// Connected, an error is returned.
func (fc *FrontendClient) WiredAuthenticate8021XWithSaved(name string) (bool, error) {
	conf := &Wired8021XConfiguration{
		Name: name,
	}

	r := &WiredAuthenticate8021XRequest{
		Config:                conf,
		TrySavedConfiguration: true,
	}

	ctx, cancel := context.WithTimeout(context.Background(), fc.timeout)
	defer cancel()

	reply, err := fc.c.WiredAuthenticate8021X(ctx, r)
	if err != nil {
		return true, err
	}

	if reply.NoSavedConfiguration {
		return false, nil
	}

	if reply.State != WiredState_WIRED_CONNECTED {
		return true, errors.Errorf("saved configuration failed for %s", name)
	}

	return true, nil
}
